// console.log("Hello World");


// Arithmetic Operators

/*
	+ sum
	- subtraction
	* multiplication
	/ division
	% modulo (returns the remainder)

*/
let x = 45;
let y = 28;

let sum = x + y;
	console.log("Result of Addition: " + sum);

let difference = x - y;
	console.log("Result of Subtraction: " + difference);

let product = x * y;
	console.log("Result of Multiplication: " + product);

let quotient = x / y;
	console.log("Result of Division: " + quotient);

let mod = x % y;
	console.log("Result of Modulo: " + mod);

// Assignment Operator
/*
	Basic assignment operator (=)
*/

let assignmentNumber = 8;


// Arithmetic Assignment Operator
/*
	addition assignment operator (+=)
	subtraction assignment operator (-=)
	multiplication assignment operator (*=)
	division assignment operator (/=)
	modulo assignment operator (%=)
*/

assignmentNumber = assignmentNumber + 2;
console.log("Result of addition operator: " + assignmentNumber);

assignmentNumber += 2;
console.log("assignmentNumber: " + assignmentNumber);

// PEMDAS

/*
	P = parenthesis
	E = Exponent
	M = Multiplication
	D = division
	A = addition
	s = subtraction
*/

let mdas = 1 + 2 - 3 * 4 / 5;
console.log("Result of mdas: " + mdas);

let pemdas = 1 + (2 - 3) * (4 / 5);
console.log("Result of pemdas: " + pemdas);

// increment and decrement
	/*
	Operators that add or subtract values by 1 
	or reassigns the value of the variable where the increment/
	decrement was applied to
	*/

let z = 1;
console.log(z);

let increment = ++z; // increment = 1 + z
console.log("Pre-increment: " + increment);
// the value of z was also increased even thouch we didn't 
// implicitly specify any value reaasignment
console.log("The Value of z: " + z);

// the value of z is returned and stored in the variable increment then the value of z is increased by 1
increment = z++;
console.log("Post-increment: " + increment);
console.log("Value of z: " + z);

let decrement = --z;
console.log("Pre-decrement: " + z);

decrement = z--;
console.log("Post-decrement: " + decrement);

// type of coercion
/*
	type coercion is the automatic or implicit conversion of values from one data type to another
		> this happen when operations are performed on different data types that would normally not be possible and yield irregular
		> values are automatically converted from one data type to another in order to resolve operations
*/

let numA = '10';
let numB = 12;

// adding/concatenating a string and a number will result in a string
let coercion = numA + numB;
console.log(coercion);
console.log(typeof coercion);

let numC = 16;
let numD = 14;
let nonCoercion = numC + numD;
console.log(nonCoercion);


// the boolean ture is also associated with the value of 1

let numE = true + 1;
console.log(numE);

let numF = false + 2;
console.log(numF);


// equality operator ==
// 	> checks whether the operance are equal / have the same contenet
console.log(1 == 1); // true
console.log(1 == 2); // false
console.log(1 == '1'); // true

// Strict Equality ===
//  > checks whether the operands are equal/have the same content
//  > also compare the data type of 2 values
//  > JavaScript is a loosely typed language meaning that values of different data type can be stored in variables
//  > in combination with type coercion, this sometimes created problems within our code eq. java, typescript
//  > strict equality operators are better to use in most cases to ensure that data types provided are correct
console.log(1 === 1); // true
console.log(1 === 2); // fales
console.log(1 === '1'); // false


// inequality operator !=
//  >checks the operands are not euql/have different content
//  >attempts to convert and compare operands of different data type
console.log(1 != 1); //false
console.log(1 != 2); //true
console.log(1 != '1'); //false

// strict inequality operator !==
console.log(1 !== 1); //false
console.log(1 !== '1'); //true

// relational operator <,>,=
//  >mostly used in conditional/comparison operators
//  > check whether one value is greater than or less than to the other value.
//  > like in equality comparison operators, relational operators also return boolean which we can save in a variable or use in a conditional statement
let a = 50;
let b = 65;

// GT or greater than >

let isGreaterThan = a > b;
console.log(isGreaterThan); // false

// LT or Less than <
let isLessThan = a < b;
console.log(isLessThan); // true

// GTE or greater than or equal to >=
let GTE = a >= b;
console.log(GTE); // false

// LTE or Less than or equal to <=
let LTE = a <= b;
console.log(LTE); // true

// forced coercion to change string to a number
let numStr = '30'
console.log(a > numStr); //true

// 
let str= 'twenty';
console.log(b >= str); // false

// logical operator
//  >and operator &&
		// returns True if all operands are ture


let isAdmin = false;
let isRegistered = true;
let isLegalAge = true;

let authorization1 = isAdmin && isRegistered;
console.log(authorization1); //false

let voteAuthorization = isLegalAge && isRegistered;
console.log(voteAuthorization); //ture

let adminAccess = isAdmin && isLegalAge && isRegistered;
console.log(adminAccess);//false

let random = isAdmin && false;
console.log(random); // false

let requiredLevel = 95;
let requiredAge = 18;

let gameTopPlayer = isRegistered && requiredLevel === 25;
console.log(gameTopPlayer);//false

let gamePlayer = isRegistered && isLegalAge && requiredLevel >=25;
// console.log(requiredLevel >=25);
console.log(gamePlayer);

let userName = 'kookie2022';
let userName2 = 'gamer01';
let userAge = 15;
let userAge2 = 26;

let registration1 = userName.length > 8 && userAge >= requiredAge;
console.log(registration1);

let registration2 = userName.length >8 && userAge2 >= requiredAge;
console.log(registration2);

//  >or operator ||
		// returns if one is ture the operands are ture

let userLevel = 100;
let userLevel2 = 65;

let guildRequirement = isRegistered || userLevel >= requiredLevel || userAge >= requiredAge;
console.log(guildRequirement);

let guildAdmin = isAdmin || userLevel2 >= requiredLevel;
console.log(guildAdmin);

// not operator ! > it returns opposite value
let guildAdmin2 = !isAdmin || userLevel2 >= requiredLevel;
console.log(guildAdmin2);

console.log(isRegistered);


// mini activity

// console.log('Hello World');

// const firstNumber  = prompt("Please enter first number");
// const secondNumber  = prompt("Please enter second number");

// const total = firstNumber + secondNumber;
// let result;

// if( total < 10){
//     result = total;
// }else if(total >= 10 &&  total < 21 ){
//     result = firstNumber - secondNumber;
// }else if( total >= 21 && total < 30){
//     result = firstNumber * secondNumber;
// }else if(total >= 30){
//     result = firstNumber / secondNumber;
// }

// if(total >= 10){
//     alert('total is: ' + total);
// }else if(total < 10){
//     console.log('total is: ' + total);
// }

// const username  = prompt("Please enter your name");
// const age = prompt("Please enter your age");

// if(username === ''  age === ''  username ===  null || age === null){
//     console.log('are you a time traveler?');
// }else if(username && age){
//     console.log('Name: ' + username + ' Age: ' + age);
// }

// function checkLegalAge(age){
//     if(age >= 18){
//         console.log('You are of legal age');
//     }else{
//         console.log('You are not allowed here');
//     }
// }

// checkLegalAge(age);

// switch (age) {
//     case 18:
//       console.log("You are not allowed to party");
//       break;
//     case 21:
//         console.log("You are now part of the adult society");
//         break;
//     case 65:
//         console.log("We thank you for your contribution to society");
//         break;
//     default:
//       console.log("We can't identify your group age");
//       break;
//   }